from langchain.llms import OpenAI

from llama_index import VectorStoreIndex, SimpleDirectoryReader


api_key="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"

llm = OpenAI(openai_api_key=api_key)

documents = SimpleDirectoryReader("data").load_data()
index = VectorStoreIndex.from_documents(documents)

query_engine = index.as_query_engine()
response = query_engine.query("What did the author do growing up?")
print(response)