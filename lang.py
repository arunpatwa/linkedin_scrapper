from langchain.llms import OpenAI
from langchain.schema import HumanMessage
from langchain.chat_models import ChatOpenAI
from langchain.prompts import PromptTemplate
from langchain.prompts.chat import ChatPromptTemplate

from langchain.schema import BaseOutputParser

#import os
#os.environ[openai_api_key="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"]

api_key="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"

#llm = OpenAI(api_key=api_key)

chat_model = ChatOpenAI(openai_api_key=api_key)

llm = OpenAI(openai_api_key=api_key)

text = "What would be a good company name for a company that makes colorful socks?"

messages = [HumanMessage(content=text)]

#print(text)

llm.invoke(text)

chat_model.invoke(messages)

prompt = PromptTemplate.from_template("What is a good name for a company that makes {product}?")
prompt.format(product="colorful socks")

print(prompt)

template = "You are a helpful assistant that translates {input_language} to {output_language}."
human_template = "{text}"

print(template)


chat_prompt = ChatPromptTemplate.from_messages([
    ("system", template),
    ("human", human_template),
])

chat_prompt.format_messages(input_language="English", output_language="French", text="I love programming.")

print(chat_prompt)



class CommaSeperatedListOutputParser(BaseOutputParser):
    """parse the output of the LLM call"""
    
    def parse(self,text: str):
        """parse output of the LLM call"""
        return text.strip().split(", ")
    
CommaSeperatedListOutputParser().parse("Hi, Bye ")
