from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import copy
import time

from bs4 import BeautifulSoup
from lxml import html


options = Options()

# options.add_argument('--headless')
# options.add_argument('--no-sandbox')

options.add_argument('--disable-dev-shm-usage')

driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)


driver.get('https://www.linkedin.com/home')

def profile_login():
    mail="USERNAME"
    password="PASSWORD"
    
    driver.find_element(By.XPATH,"//input[@type='text']").send_keys(mail)
    driver.find_element(By.XPATH,"//input[@type='password']").send_keys(password)
    
    driver.find_element(By.XPATH,"//button[@data-id='sign-in-form__submit-btn']").click()


profile_login()

def process_linkedin_url(linkedin_url):
    print(f"Processing Linkedin URL: {linkedin_url}")


user_agent='Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'

headers = {'User-Agent': user_agent}

url = driver.page_source

main_soup = BeautifulSoup(url,'lxml')

# defining all the properties 
first_name=""
last_name=""
full_name=""
followers_count=0
connections=0
profile_image_url=""
background_cover_image_url=""
city=""
state=""
country=""
country_full_name=""
summary=""
headline=""
contact_info_details=""
experiences=[]
projects=[]



#minimise the message window

def minimise_message_window():
    controls_div = driver.find_element(By.CLASS_NAME, "msg-overlay-bubble-header__controls")
    last_button = controls_div.find_elements(By.TAG_NAME, "button")[-1]
    last_button.click()
    
    print("Message Overlay window get minimised")


minimise_message_window()

def user_names():
    soup=copy.deepcopy(main_soup)
    
    global full_name , first_name , last_name
    
    full_name=soup.find('h1',class_='text-heading-xlarge inline t-24 v-align-middle break-words').get_text(strip=True)
    last_name=full_name.split()[-1]
    first_name=full_name.split()[0]

    print(first_name)
    print(last_name)
    print(full_name)
    
    return first_name,last_name,full_name

first_name,last_name,full_name=user_names()

def get_headline():
    soup=copy.deepcopy(main_soup)
    
    global headline
    if(soup.find('div',class_='text-body-medium break-words')):
        headline=soup.find('div',class_='text-body-medium break-words').get_text(strip=True)
        print(headline)
        return headline
        
    return ""

headline=get_headline()

def get_summary():
    soup=BeautifulSoup(driver.page_source,'lxml')
    global summary
    if(soup.find('div',class_='display-flex ph5 pv3')):
        summary=soup.find('div',class_='display-flex ph5 pv3').get_text(strip=True)
        print(summary)
        return summary

    return ""
summary=get_summary()

def get_highlights():

    highlight_soup=BeautifulSoup(driver.page_source, 'html.parser')

    if(highlight_soup.find('div',class_='pvs-list__outer-container')):
        highlights_details=highlight_soup.find('div',class_='pvs-list__outer-container')
        
        if(highlights_details.find('div',class_='ppsZsJKymLFlJUSOceObOsHEYkrXkSuVhVQg pvs-entity--padded JvHHhNIExyEKobNUWiOPChZtcnCYXSDmw')):
            highlights=highlights_details.find('div',class_='ppsZsJKymLFlJUSOceObOsHEYkrXkSuVhVQg pvs-entity--padded JvHHhNIExyEKobNUWiOPChZtcnCYXSDmw')
            highlights=highlights.find('span').get_text(strip=True)
            print(highlights)
            return highlights
    return ""

highlights=get_highlights()

def followers_connections():
    soup=BeautifulSoup(driver.page_source,'lxml')
    global followers_count , connections
    
    if(soup.find('ul',class_='EDYxhKVrVefqKqvpSjiiBAxtoUGczhbHRdGIEDw pv-top-card--list-bullet')):
        fs=soup.find('ul',class_='EDYxhKVrVefqKqvpSjiiBAxtoUGczhbHRdGIEDw pv-top-card--list-bullet')
        connections_details=fs.find_all('li')

        followers_count=connections_details[0].find('span').get_text(strip=True)
        connections=connections_details[1].find('span',class_="t-bold").get_text(strip=True)
        
        print(followers_count)
        print(connections)
        
        return followers_count,connections
    return 0,0


followers_count,connections=followers_connections()

def location_details():
    soup=copy.deepcopy(main_soup)
    # global city , state , country
    if(soup.find('div',class_='vEBOKgksgadKrpghoOSCroigFIfhUPMXuRJWM mt2')):
        cont=soup.find('div',class_='vEBOKgksgadKrpghoOSCroigFIfhUPMXuRJWM mt2')
        location_parts=cont.find('span').text.strip().split(",")
        city = location_parts[0].strip()
        state = location_parts[1].strip()
        country = location_parts[2].strip()

        print(city)
        print(state)
        print(country)
        
        return city,state,country
    
    return "","",""

city,state,country=location_details()

    


def all_contact_details():
    new_soup=BeautifulSoup(driver.page_source,'lxml')
    
    global contact_info_details,linkedin_url

    if(new_soup.find(class_='pv-profile-section__section-info section-info')):
        contact_info = new_soup.find(class_='pv-profile-section__section-info section-info')   
        if(contact_info.find('a')['href']):
            linkedin_url=contact_info.find('a')['href']

        contact_info=contact_info.text.replace('\n','  ')

        contact_info_details=contact_info.replace("\u2019","'").strip()
    
        print(contact_info_details)
        return contact_info_details,linkedin_url
    
    driver.back()
    return "",""
    


contact_soup=BeautifulSoup(driver.page_source,'lxml')
def contact_details():
    contact_info_elements=driver.find_element(By.ID,'top-card-text-details-contact-info')
    contact_info_elements.click()
    try:
        contact_present=EC.presence_of_element_located((By.ID,'top-card-text-details-contact-info'))
        WebDriverWait(driver,10).until(contact_present)
    except TimeoutException:
        print("Timed out waiting for page to load")
    finally:
        print("Page loaded")
        time.sleep(2)
        all_contact_details()
    

contact_details()

def click_show_all_experience():
    experince_soup=BeautifulSoup(driver.page_source,'lxml')

    link=experince_soup.find('a',{'id':'navigation-index-see-all-experiences'}).get('href')

    show_all_experience=driver.find_element(By.XPATH,"//a[@id='navigation-index-see-all-experiences']")

    print(link)
    
    show_all_experience.click()
    

click_show_all_experience()
time.sleep(4)

# Trying to get all the experiences once using for loop
def get_all_experience_details():
    new_soup=BeautifulSoup(driver.page_source,'lxml')
    global experiences
    experiences=[]

    all_experience=new_soup.find_all('li',class_='pvs-list__paged-list-item artdeco-list__item pvs-list__item--line-separated pvs-list__item--one-column')
    for experience in all_experience: 
        if experience.find('div', class_='ppsZsJKymLFlJUSOceObOsHEYkrXkSuVhVQg pvs-entity--padded JvHHhNIExyEKobNUWiOPChZtcnCYXSDmw'):
            if(experience.find('div', class_='display-flex align-items-center mr1 t-bold')):
                title = experience.find('div', class_='display-flex align-items-center mr1 t-bold').find('span').get_text(strip=True)
                print(title)
            
            if(experience.find('div',class_='display-flex flex-column full-width')):
                company_details=experience.find('div',class_='display-flex flex-column full-width')
                company=company_details.find('span',class_='t-14 t-normal').find('span').get_text(strip=True)  
                company=company.replace('\u00b7',"")
                print(company)

                company_span=company_details.find_all('span',class_='t-14 t-normal t-black--light')

                start_end=company_span[0]

                start_end_result = start_end.find('span', class_='pvs-entity__caption-wrapper').get_text(strip=True)
                start_end_result=start_end_result.replace('\u00b7',"")
                print(start_end_result)

                location_details=company_span[1]
                location=location_details.find('span').get_text(strip=True)
                location=location.replace('\u00b7',"")
                print(location)
            
                
            descriptions=""
              
            if(experience.find('div',class_='pvs-list__outer-container pvs-entity__sub-components')):
                all_description_details=experience.find('li',class_='TPbZDJYZHPlKCdaCvYdpSYAYPoBvcsnjmfxA')
                descriptions=all_description_details.find('span').text.strip()
                print(descriptions)
                    
            print('\n')
                    
            experience_data={
                "company":f'{company}',
                "company_linkedin_profile_url":f"",
                "description":f'{descriptions}',
                "ends_at":f'{start_end_result}',
                "logo_url":f'',
                "location":f'{location}',
                "start_at":{
                    "month":f'',
                    "year":f''
                },
                "title":f'{title}'
            }
            
            experiences.append(experience_data)
            
        if experience.find('div', class_='display-flex align-items-center mr1 hoverable-link-text t-bold'):
            text = experience.find('div', class_='display-flex align-items-center mr1 hoverable-link-text t-bold').find('span').get_text(strip=True)
            

    driver.back()
  
get_all_experience_details()


def profile_images_url():
    soup=copy.deepcopy(main_soup)
    global background_cover_image_url,profile_image_url
    
    if(soup.find('img',id='profile-background-image-target-image')):
        background_cover_image_url=soup.find('img',id='profile-background-image-target-image').get('src')
        print(background_cover_image_url)
        
    if(soup.find('div',class_='pv-top-card--photo text-align-left pv-top-card--photo-resize')):
        profile_pic_section=soup.find('div',class_='pv-top-card--photo text-align-left pv-top-card--photo-resize')
        profile_image_url=profile_pic_section.find('img').get('src')
        print(profile_image_url)
        
profile_images_url()    

import json 

file_path='linkedin_sc.json' 
      
new_data={
    'first_name':f'{first_name}',
    "last_name":f'{last_name}',
    'full_name':f'{full_name}',
    'headline':f'{headline}',
    'summary':f'{summary}',
    'linkedin_url':f'{linkedin_url}',
    'contact_info':f"""{contact_info_details}""",
    "follower_count":f'{followers_count}',
     "connections": f"{connections}",
    'background_cover_image_url':f'{background_cover_image_url}',
    'profile_pic_url':f'{profile_image_url}',
    "city": f"{city}",
    "country": f"{country}",
    "experiences":experiences,
    "accomplishment_projects":projects
    }

with open(file_path, 'w') as file:
    json.dump(new_data, file,indent=2)

print("Data inserted successfully.")
