import streamlit as st
from other_file import process_linkedin_url
import json
from urllib.parse import urlparse

def extract_username_from_linkedin_url(linkedin_url):
    parsed_url= urlparse(linkedin_url)
    path_parts = parsed_url.path.split('/')
    username = path_parts[-1] if path_parts[-1] else path_parts[-2]
    return username


def main():
    st.title("Company and Receiver Details")

    default_company_name = "PlugSeal Innovation and Services Limited"
    default_company_description = """Plugseal Innovation Services Private Limited (PISPL) is a Private Limited Indian Non-Government Company incorporated in India on 24 September 2020 (Three years and three months 12 days old ). Its registered office is in Central Delhi, Delhi, India.

The Company is engaged in the Healthcare Industry.

The Company's status is Active, and it has filed its Annual Returns and Financial Statements up until 31 March 2022. It's a company limited by shares with an authorized capital of Rs 10.00 Lakh and a paid-up capital of Rs 10.00 Lakh, as per the Ministry of Corporate Affairs (MCA) records."""
    
    default_product_description = "We are selling a product and services"

    default_sender_name = "Vishwajeet Kumar"


    st.sidebar.header("Company Details")
    company_name = st.sidebar.text_input("Company Name", default_company_name)
    company_description = st.sidebar.text_area("Company Description", default_company_description, height=350)
    company_user = st.sidebar.text_input("Company User", default_sender_name)
    product_description = st.sidebar.text_area("Product Description", default_product_description, height=100)

    st.sidebar.header("Receiver Details")
    
    linkedin_url = st.sidebar.text_input("Linkedin URL", "")

    st.session_state.linked_url = linkedin_url
    
    st.header("Entered Details:")
    st.write(f"Company Name: {company_name}")
    st.write(f"Company Description: {company_description}")
    st.write(f"Product Description: {product_description}")
    st.write(f"Company User: {company_user}")


    if linkedin_url!="":
        user_id_name=extract_username_from_linkedin_url(linkedin_url)
        st.header(f"JSON Content from {user_id_name}.json")
        try:
            json_file_path = f'./json_details/{user_id_name}.json'
            with open(json_file_path,"r") as json_file:
                json_content = json.load(json_file)
                st.json(json_content)
        except FileNotFoundError:
            st.warning(f"The {user_id_name} json file is not found.")
        
        import email_generation   
        st.header("Generated Cold Email Content")
        try:
            email_content=email_generation.generated_cold_email
            # st.text_area(,height=400)
            st.markdown(f'<div style="width: 800px">{email_content}</div>', unsafe_allow_html=True)
        except FileNotFoundError:
            st.warning("The Cold Email is not Generated Succesfully")
        
        
    # print(email_generation.generated_cold_email)
    
    
    

if __name__ == "__main__":
    main()
