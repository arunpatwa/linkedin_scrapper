from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
from selenium.common.exceptions import TimeoutException
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
import time
import copy


def initialize_driver():
    options = Options()
    options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)
    return driver

def login(driver, username, password):
    driver.get('https://www.linkedin.com/home')
    
    driver.find_element(By.XPATH, "//input[@type='text']").send_keys(username)
    driver.find_element(By.XPATH, "//input[@type='password']").send_keys(password)
    
    driver.find_element(By.XPATH, "//button[@data-id='sign-in-form__submit-btn']").click()

def minimize_message_window(driver):
    controls_div = driver.find_element(By.CLASS_NAME, "msg-overlay-bubble-header__controls")
    last_button = controls_div.find_elements(By.TAG_NAME, "button")[-1]
    last_button.click()
    
    print("Message Overlay window get minimized")
    
    

def get_profile_data(driver):
    
    linkedin_url="https://www.linkedin.com/in/vishwajeet1106/"
    driver.get(linkedin_url)
    
    url = driver.page_source
    
    main_soup = BeautifulSoup(url, 'lxml')
    
    user_names(main_soup)
    
    get_headline(main_soup)
    get_summary(driver)
    get_highlights(driver)
    followers_count, connections = followers_connections(driver)
    location_details(main_soup)
    all_contact_details(driver)
    contact_details(driver)
    click_show_all_experience(driver)
    get_all_experience_details(driver)
    profile_images_url(main_soup)

def user_names(soup):
    new_soup=BeautifulSoup()
    full_name = soup.find('h1', class_='text-heading-xlarge inline t-24 v-align-middle break-words').get_text(strip=True)
    last_name = full_name.split()[-1]
    first_name = full_name.split()[0]
    
    print(first_name)
    print(last_name)
    print(full_name)
    
    return first_name, last_name,full_name
    

def get_headline(soup):
    global headline
    if soup.find('div', class_='text-body-medium break-words'):
        headline = soup.find('div', class_='text-body-medium break-words').get_text(strip=True)
        print(headline)

def get_summary(driver):
    soup = BeautifulSoup(driver.page_source, 'lxml')
    if soup.find('div', class_='display-flex ph5 pv3'):
        summary = soup.find('div', class_='display-flex ph5 pv3').get_text(strip=True)
        print(summary)



def get_highlights(driver):
    highlight_soup = BeautifulSoup(driver.page_source, 'html.parser')

    if highlight_soup.find('div', class_='pvs-list__outer-container'):
        highlights_details = highlight_soup.find('div', class_='pvs-list__outer-container')

        if highlights_details.find('div', class_='ppsZsJKymLFlJUSOceObOsHEYkrXkSuVhVQg pvs-entity--padded JvHHhNIExyEKobNUWiOPChZtcnCYXSDmw'):
            highlights = highlights_details.find('div', class_='ppsZsJKymLFlJUSOceObOsHEYkrXkSuVhVQg pvs-entity--padded JvHHhNIExyEKobNUWiOPChZtcnCYXSDmw')
            highlights = highlights.find('span').get_text(strip=True)
            print(highlights)

def followers_connections(driver):
    soup = BeautifulSoup(driver.page_source, 'lxml')
    followers_count, connections = 0, 0

    if soup.find('ul', class_='EDYxhKVrVefqKqvpSjiiBAxtoUGczhbHRdGIEDw pv-top-card--list-bullet'):
        fs = soup.find('ul', class_='EDYxhKVrVefqKqvpSjiiBAxtoUGczhbHRdGIEDw pv-top-card--list-bullet')
        connections_details = fs.find_all('li')

        followers_count = connections_details[0].find('span').get_text(strip=True)
        connections = connections_details[1].find('span', class_="t-bold").get_text(strip=True)

        print(followers_count)
        print(connections)

    return followers_count, connections

def location_details(main_soup):
    city, state, country = "", "", ""
    if main_soup.find('div', class_='vEBOKgksgadKrpghoOSCroigFIfhUPMXuRJWM mt2'):
        cont = main_soup.find('div', class_='vEBOKgksgadKrpghoOSCroigFIfhUPMXuRJWM mt2')
        location_parts = cont.find('span').text.strip().split(",")
        city = location_parts[0].strip()
        state = location_parts[1].strip()
        country = location_parts[2].strip()

        print(city)
        print(state)
        print(country)

    return city, state, country


def all_contact_details(driver):
    new_soup = BeautifulSoup(driver.page_source, 'lxml')
    contact_info_details, linkedin_url = "", ""

    if new_soup.find(class_='pv-profile-section__section-info section-info'):
        contact_info = new_soup.find(class_='pv-profile-section__section-info section-info')
        if contact_info.find('a')['href']:
            linkedin_url = contact_info.find('a')['href']

        contact_info = contact_info.text.replace('\n', '  ')

        contact_info_details = contact_info.replace("\u2019", "'").strip()

        print(contact_info_details)

    driver.back()
    return contact_info_details, linkedin_url

def contact_details(driver):
    contact_info_elements = driver.find_element(By.ID, 'top-card-text-details-contact-info')
    contact_info_elements.click()
    try:
        contact_present = EC.presence_of_element_located((By.ID, 'top-card-text-details-contact-info'))
        WebDriverWait(driver, 10).until(contact_present)
    except TimeoutException:
        print("Timed out waiting for page to load")
    finally:
        print("Page loaded")
        time.sleep(2)
        return all_contact_details(driver)

def click_show_all_experience(driver):
    experince_soup = BeautifulSoup(driver.page_source, 'lxml')

    link = experince_soup.find('a', {'id': 'navigation-index-see-all-experiences'}).get('href')

    show_all_experience = driver.find_element(By.XPATH, "//a[@id='navigation-index-see-all-experiences']")

    print(link)

    show_all_experience.click()

def get_all_experience_details(driver):
    new_soup = BeautifulSoup(driver.page_source, 'lxml')
    experiences = []

    all_experience = new_soup.find_all('li',
                                       class_='pvs-list__paged-list-item artdeco-list__item pvs-list__item--line-separated pvs-list__item--one-column')
    for experience in all_experience:
        if experience.find('div', class_='ppsZsJKymLFlJUSOceObOsHEYkrXkSuVhVQg pvs-entity--padded JvHHhNIExyEKobNUWiOPChZtcnCYXSDmw'):
            if experience.find('div', class_='display-flex align-items-center mr1 t-bold'):
                title = experience.find('div', class_='display-flex align-items-center mr1 t-bold').find('span').get_text(
                    strip=True)
                print(title)

            if experience.find('div', class_='display-flex flex-column full-width'):
                company_details = experience.find('div', class_='display-flex flex-column full-width')
                company = company_details.find('span', class_='t-14 t-normal').find('span').get_text(strip=True)
                company = company.replace('\u00b7', "")
                print(company)

                company_span = company_details.find_all('span', class_='t-14 t-normal t-black--light')

                start_end = company_span[0]

                start_end_result = start_end.find('span', class_='pvs-entity__caption-wrapper').get_text(strip=True)
                start_end_result = start_end_result.replace('\u00b7', "")
                print(start_end_result)

                location_details = company_span[1]
                location = location_details.find('span').get_text(strip=True)
                location = location.replace('\u00b7', "")
                print(location)

            descriptions = ""

            if experience.find('div', class_='pvs-list__outer-container pvs-entity__sub-components'):
                all_description_details = experience.find('li', class_='TPbZDJYZHPlKCdaCvYdpSYAYPoBvcsnjmfxA')
                descriptions = all_description_details.find('span').text.strip()
                print(descriptions)

            print('\n')

            experience_data = {
                "company": f'{company}',
                "company_linkedin_profile_url": f"",
                "description": f'{descriptions}',
                "ends_at": f'{start_end_result}',
                "logo_url": f'',
                "location": f'{location}',
                "start_at": {
                    "month": f'',
                    "year": f''
                },
                "title": f'{title}'
            }

            experiences.append(experience_data)

        if experience.find('div', class_='display-flex align-items-center mr1 hoverable-link-text t-bold'):
            text = experience.find('div', class_='display-flex align-items-center mr1 hoverable-link-text t-bold').find(
                'span').get_text(strip=True)

    driver.back()
    return experiences


def profile_images_url(soup):
    global background_cover_image_url, profile_image_url

    if soup.find('img', id='profile-background-image-target-image'):
        background_cover_image_url = soup.find('img', id='profile-background-image-target-image').get('src')
        print(background_cover_image_url)

    if soup.find('div', class_='pv-top-card--photo text-align-left pv-top-card--photo-resize'):
        profile_pic_section = soup.find('div', class_='pv-top-card--photo text-align-left pv-top-card--photo-resize')
        profile_image_url = profile_pic_section.find('img').get('src')
        print(profile_image_url)
        



driver = initialize_driver()
login(driver, "USERNAME", "PASSWORD")
minimize_message_window(driver)

get_profile_data(driver)

followers_count, connections = followers_connections(driver)
city, state, country = location_details(BeautifulSoup(driver.page_source, 'lxml'))

contact_info_details, linkedin_url = contact_details(driver)
click_show_all_experience(driver)
time.sleep(4)
experiences = get_all_experience_details(driver)

# driver.quit()
# 