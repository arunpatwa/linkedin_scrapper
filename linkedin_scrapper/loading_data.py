import json

def load_data(json_file_path):
    with open(json_file_path, 'r') as file:
        data = json.load(file)

    return data

def person_details(data):
    full_name = data.get('full_name', 'N/A')
    headline = data.get('headline', 'N/A')
    summary = data.get('summary', 'N/A')
    city = data.get('city', 'N/A')
    country = data.get('country', 'N/A')
    contact_info = data.get('contact_info', 'N/A')

    template = "I am {full_name},{summary}. I belong to {city} in {country}. My basic info contains {headline} "

    formatted_document = template.format(full_name=full_name, summary=summary, city=city, country=country, headline=headline, contact_info=contact_info)

    return formatted_document

if __name__ == "__main__":
    json_file_path = './linkedin_sc.json'
    data = load_data(json_file_path)
    formatted_document = person_details(data)
    print(formatted_document)
