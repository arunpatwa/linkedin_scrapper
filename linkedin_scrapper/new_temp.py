from urllib.parse import urlparse

def extract_username_from_linkedin_url(linkedin_url):
    # Parse the LinkedIn URL
    parsed_url = urlparse(linkedin_url)

    # Split the path and get the last part (username)
    path_parts = parsed_url.path.split('/')
    username = path_parts[-1] if path_parts[-1] else path_parts[-2]

    return username

# Example usage
linkedin_url = "https://www.linkedin.com/in/vazmeee/"

user_id_name=extract_username_from_linkedin_url(linkedin_url)