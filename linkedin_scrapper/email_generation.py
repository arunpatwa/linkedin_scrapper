# %%
from langchain.document_loaders import UnstructuredURLLoader
from langchain.chains.summarize import load_summarize_chain
from langchain.llms import OpenAI 
from langchain.chains import LLMChain
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.prompts import PromptTemplate
from loading_data import load_data, person_details
import json
from langchain.chains.summarize import load_summarize_chain

from langchain_core.documents import Document


import os
from dotenv import load_dotenv
load_dotenv()
from urllib.parse import urlparse

# %%
openai_api_key = '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'

llm=OpenAI(openai_api_key=openai_api_key,temperature=0.6)

# also need to pass typed linkedin url here...this can be also get from the json file...

def extract_username_from_linkedin_url(linkedin_url):
    parsed_url = urlparse(linkedin_url)
    path_parts = parsed_url.path.split('/')
    username = path_parts[-1] if path_parts[-1] else path_parts[-2]

    return username

linkedin_url = "https://www.linkedin.com/in/vazmeee/"

user_id_name=extract_username_from_linkedin_url(linkedin_url)

json_file_path = f'./json_details/{user_id_name}.json'


with open(json_file_path, 'r') as file:
    data = json.load(file)

full_name = data.get('full_name', 'N/A')
headline = data.get('headline', 'N/A')
summary = data.get('summary', 'N/A')
city = data.get('city', 'N/A')
country = data.get('country', 'N/A')
contact_info=data.get('contact_info','N/A')

template = "I am {full_name},{summary}. I belong to {city} in {country}. My basic info contains {headline} "
formatted_document = template.format(full_name=full_name, summary=summary, city=city, country=country,headline=headline,contact_info=contact_info)
# %%
data = [Document(page_content=f"{formatted_document}")]
print(data)
print (f"Preview of your data:\n\n{data[0].page_content[:40]}")

# %%
text_splitter = RecursiveCharacterTextSplitter(
    chunk_size = 500,
    chunk_overlap  = 0
)
# %%
docs = text_splitter.split_documents(data)

print (f"You now have {len(docs)} documents")

map_prompt_template=""" Below is the about section of the {reciever_name} 

write a concise summary of {reciever_name} from all the section.If the information does not include about that person details , exclude it from the summary.
{text}

CONCISE_SUMMARY:
"""
map_prompt=PromptTemplate(template=map_prompt_template,input_variables=["reciever_name","text"])


map_chain=LLMChain(llm=llm,prompt=map_prompt)

# %%
combined_prompt_template="""

{sender_name} is the sales rep at {sender_company} and sending a personalised cold email to {reciever_name} . 
{sender_name}  wanted to sell his product to the {reciever_name} by giving context how his experience 
and work related to the product that {sender_name} selling. 

INFORMATION ABOUT {sender_company} WHO IS SENDING THE COLD EMAIL:
{sender_company_details}

INFORMATION ABOUT THE PERSON {reciever_name} WHO IS RECIEVING THE EMAIL:
{text}
end the email with regards and thanking him.

YOUR RESPONSE:

"""
combine_prompt=PromptTemplate(template=combined_prompt_template,input_variables=
                                        ["sender_name","sender_company","sender_company_details","reciever_name","text"])

combined_chain=LLMChain(llm=llm,prompt=combine_prompt)

# %%
sender_company_details = """
Plugseal Innovation Limited is a digital age enabler for businesses. 
They help define and develop products through a blend of strategy, design, and technology expertise. 
By working closely with clients, they build tailored solutions from initial vision to market launch, empowering businesses of all sizes to thrive in the digital landscape. 
Think of them as your digital product co-pilot, taking you from brainstorm to brilliant.
"""

sender_name="Arun Patwa"
reciever_name=full_name
#fetch it from streamlit 
sender_company="Plugseal Innovation Limited"

# %%
llm = OpenAI(temperature=.3, openai_api_key=openai_api_key,model='gpt-3.5-turbo-instruct')

chain = load_summarize_chain(llm,
                             chain_type="map_reduce",
                             map_prompt=map_prompt,
                             combine_prompt=combine_prompt,
                             verbose=True
                            )
# %%
output = chain({"input_documents": docs,
                "sender_company": f"{sender_company}",
                "reciever_name" : f"{reciever_name}",
                "sender_company_details" : sender_company_details,
                "sender_name" : f"{sender_name}",
               })
# %%
generated_cold_email=(output['output_text'])

print(generated_cold_email)