from langchain.llms import OpenAI
from langchain.chains import LLMChain
from langchain.prompts import PromptTemplate

api_key="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"



def generate_names(animal_type,pet_color):
    
    llm=OpenAI(openai_api_key=api_key,temperature=0.7)
    
    #name=llm("template")
    
    prompt_template_name=PromptTemplate(
        input_variables=['animal_type','pet_color'],
        template="I have a nice {pet_color} {animal_type}, I am looking for its cool name , suggest me some good name "
    )
    
    name_chain = LLMChain(llm=llm , prompt=prompt_template_name,output_key='pet_name')
    response = name_chain({'animal_type':animal_type, 'pet_color':pet_color})
    return response

if __name__ == "__main__":
    print(generate_names("cow","black")) #"I have a nice white cat, I am looking for its cool name , suggest me some good name "