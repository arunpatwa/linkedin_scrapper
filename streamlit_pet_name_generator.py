import langchain_helper as lch
import streamlit as st


st.title("Pets name Generator")

user_animal_type=st.sidebar.selectbox("What is your pet",("Cat","Dog","Cow","Hamster"))

if user_animal_type == "Cat":
    pet_color=st.sidebar.text_area(label="what color is your Cat",max_chars=15)

if user_animal_type == "Dog":
    pet_color=st.sidebar.text_area(label="what color is your Dog",max_chars=15)

if user_animal_type == "Cow":
    pet_color=st.sidebar.text_area(label="what color is your Cow",max_chars=15)

if user_animal_type == "Hamster":
    pet_color=st.sidebar.text_area(label="what color is your Hamster",max_chars=15)
    
if pet_color:
    response=lch.generate_names(user_animal_type,pet_color)
    st.text (response['pet_name'])