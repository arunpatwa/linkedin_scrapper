import requests

# Set your API credentials
client_id = '773x3vlhd7rurw'
client_secret = 'PSBHjuhNOlKiOg1L'
redirect_uri = 'https://www.linkedin.com/in/arunpatwa-iit/'

# Get authorization code (use your own logic to obtain this)
authorization_code = 'your_authorization_code'

# Obtain access token
token_url = 'https://www.linkedin.com/oauth/v2/accessToken'
token_data = {
    'grant_type': 'authorization_code',
    'code': authorization_code,
    'redirect_uri': redirect_uri,
    'client_id': client_id,
    'client_secret': client_secret,
}

token_response = requests.post(token_url, data=token_data)
access_token = token_response.json()['access_token']

# Make API request
profile_url = 'https://api.linkedin.com/v2/me'
headers = {'Authorization': f'Bearer {access_token}'}
profile_response = requests.get(profile_url, headers=headers)

# Print the profile data
print(profile_response.json())
